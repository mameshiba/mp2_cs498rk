import React from "react";
import MovieDetail from "./MovieDetail"
import {
  Switch,
  Route,
  Link,
} from "react-router-dom";

class Movie extends React.Component {
    constructor(props) {
        super(props);

        this.handleIndexChange = this.handleIndexChange.bind(this);
    }

    handleIndexChange(e) {
        let m;
        let newindex = 0;
        for (m of this.props.movies) {
            if (m.id === this.props.movie.id) {
                break;
            }
            newindex++;
        }
        this.props.onIndexChange(newindex);
    }

    render() {
        const movie = this.props.movie;
        const path = '/detail/' + movie.id;
        console.log(path);
        return (
            <div>
                <Link to={`/detail/${movie.id}`} onClick={this.handleIndexChange}>
                    <img src={this.props.baseURL.concat(this.props.imgSize.concat(movie.poster_path))}></img>
                    <div>{movie.title}</div>
                    <div>{movie.popularity}</div>
                    <div>{movie.release_date}</div>
                    <br></br>
                </Link>

                <Switch>
                    <Route path={`/detail/${movie.id}`}>
                        <MovieDetail 
                            movies={this.props.movies} 
                            movie={movie}
                            baseURL={this.props.baseURL}
                            imgSize={this.props.imgSize}
                        />                        
                    </Route>
                </Switch>
            </div>
        );
    }
}

export default Movie;
