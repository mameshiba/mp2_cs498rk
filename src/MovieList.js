import React from "react" 
import Movie from "./Movie"
import styles from "./MovieList.module.css"

class MovieList extends React.Component {
    constructor(props) {
        super(props);
        this.handleIndexChange = this.handleIndexChange.bind(this);
    }

    handleIndexChange(newindex) {
        this.props.onIndexChange(newindex);
    }

    render() {
        const searchText = this.props.searchText;
        const sortBy = this.props.sortBy;
        const order = this.props.order;
        const moviesSorted = this.props.movies;

        const rows = [];
        moviesSorted.sort((a, b) => (a[sortBy] > b[sortBy]) ? 1 : -1)
        if (order === "decreasing") {
            moviesSorted.reverse();
        }
        moviesSorted.forEach((movie) => {
            if (typeof movie.title === 'undefined') {
                return;
            }
            if (movie.title.toLowerCase().indexOf(searchText.toLowerCase()) === -1) {
                return;
            }
            rows.push(
                <div className={styles.movie}><Movie movies={this.props.movies} movie={movie} key={movie.title} baseURL={this.props.baseURL} imgSize={this.props.imgSize} onIndexChange={this.handleIndexChange}/></div>
            );
        });
        return (
            <div className={styles.display}>
                {rows}
            </div>
        );
    }
}

export default MovieList;
