import React from "react";
import styles from './SearchBar.module.css';


class SearchBar extends React.Component {
    constructor(props) {
        super(props);
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.handleSortByChange = this.handleSortByChange.bind(this);
        this.handleOrderChange = this.handleOrderChange.bind(this);
    }
    
    handleSearchTextChange(e) {
        this.props.onSearchTextChange(e.target.value);
    }

    handleSortByChange(e) {
        this.props.onSortByChange(e.target.value);
    }

    handleOrderChange(e) {
        this.props.onOrderChange(e.target.value);
    }

    render() {
        return (
                <form>
                    <div className={styles.search}>
                        <input 
                            className={styles.searchBar}
                            type="text" 
                            placeholder="Search..." 
                            value={this.props.searchText}
                            onChange={this.handleSearchTextChange}
                        />
                    <div>
                        <select value={this.props.sortBy} onChange={this.handleSortByChange}> 
                            Sorted By:
                            <option value="title">Title</option>
                            <option value="popularity">Rank</option>
                            <option value="release_date">Date</option>
                        </select>
                    </div>
                    <div>
                        <select value={this.props.order} onChange={this.handleOrderChange}>
                            Sorting Order:
                            <option value="increasing">Increasing</option>
                            <option value="decreasing">Decreasing</option> 
                        </select>
                    </div>
                    </div>
                </form>
        );
    }
}

export default SearchBar;
