import React from 'react';
import {
  Switch,
  Route,
  Link,
} from "react-router-dom";

class MovieDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0
        };

    }
    render() {
        const movie = this.props.movie;
        return (
            <div>
                <h1>{movie.title}</h1>
                <p>Popularity: {movie.popularity}</p>
                <p>Average Rating: {movie.vote_average}</p>
                <p>Language: {movie.original_language}</p>
                <img src={this.props.baseURL.concat(this.props.imgSize.concat(movie.poster_path))}></img>
                <p>{movie.overview}</p>
                <p>Released: {movie.release_date}</p>
            </div>
        );
    }
}

export default MovieDetail;
