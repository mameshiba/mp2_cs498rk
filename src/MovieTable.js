import React from 'react';
import styles from './MovieTable.module.css';
import SearchBar from './SearchBar';
import MovieList from './MovieList';
import MovieGallery from './MovieGallery';
import Detail from './Detail';
import axios from 'axios';
import image from './movie_camera_retro2.png';
import {
  Switch,
  Route,
  Link,
} from "react-router-dom";

class MovieTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchText: "",
            sortBy: "popularity",
            order: "increasing",
            movies: [],
            baseURL: "",
            imgSize: "w342",
            loading: true,
            genreInfo:[],
            genres: [],
            index: 0
        };
            
        this.handleSearchTextChange = this.handleSearchTextChange.bind(this);
        this.handleSortByChange = this.handleSortByChange.bind(this);  
        this.handleOrderChange = this.handleOrderChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.handleIndexChange = this.handleIndexChange.bind(this);
    }

    componentDidMount() {
        axios.all([
            axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=a2df360107873ef4c3e552da9db68001&language=en-US&page=1'),
            axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=a2df360107873ef4c3e552da9db68001&language=en-US&page=2'),
            axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=a2df360107873ef4c3e552da9db68001&language=en-US&page=3'),
            axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=a2df360107873ef4c3e552da9db68001&language=en-US&page=4'),
            axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=a2df360107873ef4c3e552da9db68001&language=en-US&page=5'),
        ]).then((responseArr) => {
            this.setState({movies: responseArr[0].data.results});
            this.setState((state, props) => ({movies: state.movies.concat(responseArr[1].data.results)}));
            this.setState((state, props) => ({movies: state.movies.concat(responseArr[2].data.results)}));
            this.setState((state, props) => ({movies: state.movies.concat(responseArr[3].data.results)}));
            this.setState((state, props) => ({movies: state.movies.concat(responseArr[4].data.results)}));
            this.setState({loading: false});
        
        }, (error) => {console.log(error);}
        );
            

        axios.get('https://api.themoviedb.org/3/configuration?api_key=a2df360107873ef4c3e552da9db68001').then((response) => {
            const settings = response.data;
            this.setState({baseURL: settings.images.base_url, imgSize: settings.images.poster_sizes[3]});}, (error) => {console.log(error);});

        axios.get('https://api.themoviedb.org/3/genre/movie/list?api_key=a2df360107873ef4c3e552da9db68001&language=en-US').then((response) => {
            const genreInfo = response.data;
            this.setState({genreInfo: genreInfo.genres});}, (error) => {console.log(error);});
    }

    handleSearchTextChange(searchText) {
        this.setState({
            searchText: searchText
        });
    }
    
    handleSortByChange(sortBy) {
        this.setState({
            sortBy: sortBy
        });
    }

    handleOrderChange(order) {
        this.setState({
            order: order
        });
    }

    handleIndexChange(nindex) {
        if (nindex >= 0 && nindex < this.state.movies.length) {
            this.setState({
                index: nindex
            });
        }
        else {
            this.setState((state, props) => ({
                index: state.index
            }));
        }
    }

    handleClick(genre) {
        genre = Number(genre);
        const index = this.state.genres.indexOf(genre);

        if (index === -1) {
            this.setState(function(state, props){
                state.genres.push(genre);
                return {
                    genres: state.genres
                }
            });
        }
        else {
            this.setState((state, props) => ({
                genres: state.genres.filter(function(g) {
                    return g !== genre;
                })
            }));
        }
    }

    render() {
        let load;
        if (this.state.loading) {
            load = <div>LOADING DATA...</div>;
        }
        else {
            load = <div></div>
        }
        if (this.state.movies.length > 0) {
        return(
            <div>
                <div className={styles.heading}>
                    <img src={image} className={styles.image}/>
                    <h1 className={styles.title}>Cinema Top 100 Movies</h1>
                </div>
            <div className={styles.options}>
                <Link to="/search"> List View </Link>
                <Link to="/gallery"> Gallery View </Link>
                <Link to={`/detail/${this.state.movies[this.state.index].id}`}> Detail View </Link>
            </div>

                <Switch>
                    <Route path={`/detail/${this.state.movies[this.state.index].id}`}> 
                        <Detail 
                            movies={this.state.movies}
                            baseURL={this.state.baseURL}
                            imgSize={this.state.imgSize}
                            index={this.state.index}
                            onIndexChange={this.handleIndexChange}
                        />
                    </Route>
                    <Route path="/gallery">
                        <MovieGallery movies={this.state.movies}
                            baseURL={this.state.baseURL}
                            imgSize={this.state.imgSize}
                            genres={this.state.genres}
                            genreInfo={this.state.genreInfo}
                            onGenreChange={this.handleClick}
                            onIndexChange={this.handleIndexChange}
                        />
                    </Route>
                    <Route path="/search">
                        <SearchBar
                            sortBy={this.state.sortBy}
                            order={this.state.order}
                            searchText={this.state.searchText}
                            onSearchTextChange={this.handleSearchTextChange}
                            onSortByChange={this.handleSortByChange}
                            onOrderChange={this.handleOrderChange}
                        />
                        <MovieList movies={this.state.movies} 
                            sortBy={this.state.sortBy}
                            order={this.state.order}
                            searchText={this.state.searchText}
                            baseURL={this.state.baseURL}
                            imgSize={this.state.imgSize}
                            onIndexChange={this.handleIndexChange}
                        />
                    </Route>
                </Switch>
                {load}
            </div>
        );
        }
        else {
            return null;
        }
    }
}
export default MovieTable;




    
