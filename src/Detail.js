import React from 'react';
import styles from './Detail.module.css'
import MovieDetail from './MovieDetail';
import PropTypes from 'prop-types';
import {
  Switch,
  Route,
  Link,
} from "react-router-dom";


class Detail extends React.Component {
    constructor(props) {
        super(props);

        this.handleDetailPrevious = this.handleDetailPrevious.bind(this);
        this.handleDetailNext = this.handleDetailNext.bind(this);
    }

    handleDetailPrevious(e) {
        const newindex = this.props.index - 1;
        this.props.onIndexChange(newindex);
    }
    handleDetailNext(e) {
        const newindex = this.props.index + 1;
        this.props.onIndexChange(newindex);
    }
    
    render() {
        console.log(this.props.index);
        if (this.props.movies.length > 0) {
            console.log(this.props.movies);
            let movie = this.props.movies[this.props.index];
            let prev = movie;
            let next = movie;

            if (this.props.index > 0) {
                prev = this.props.movies[this.props.index - 1];
            }
            if (this.props.index < this.props.movies.length-1) {
                next = this.props.movies[this.props.index + 1];
            }
            return (
                <div>
                    <div className={styles.buttons}>
                        <Link to={`/detail/${prev.id}`} onClick={this.handleDetailPrevious}>
                            Previous
                        </Link>
                        <div className={styles.space}></div>
                        <Link to={`/detail/${next.id}`} onClick={this.handleDetailNext}>
                            Next
                        </Link>
                    </div>
                    <div className={styles.movie}>
                        <MovieDetail 
                            movies={this.props.movies} 
                            movie={movie}
                            baseURL={this.props.baseURL}
                            imgSize={this.props.imgSize}
                        />                        
                    </div>

                    <Switch>
                        <Route exact path='/detail'>
                            <h3>Select a Movie</h3>
                        </Route>
                        <Route path={`/detail/${next.id}`}>
                            <MovieDetail 
                                movies={this.props.movies} 
                                movie={next}
                                baseURL={this.props.baseURL}
                                imgSize={this.props.imgSize}
                            />                        
                        </Route>
                        <Route path={`/detail/${prev.id}`}>
                            <MovieDetail 
                                movies={this.props.movies} 
                                movie={prev}
                                baseURL={this.props.baseURL}
                                imgSize={this.props.imgSize}
                            />                        
                        </Route>
                    </Switch>
                </div>


            );
        }  
        return null;
    }
}

Detail.propTypes = {
  baseURL: PropTypes.string  
};

export default Detail;

