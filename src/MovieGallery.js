import React from 'react';
import Movie from "./Movie";
import styles from './MovieGallery.module.css';


class MovieGallery extends React.Component {
    constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.handleIndexChange = this.handleIndexChange.bind(this);
    }

    handleClick(e) {
        this.props.onGenreChange(e.target.value);
    }

    handleIndexChange(newindex) {
        this.props.onIndexChange(newindex);
    }

    render() {
        const buttons = this.props.genreInfo.map((genre) =>
            <button className={styles.button} color="primary" value={genre.id} key={genre.id} onClick={this.handleClick}>
                {genre.name}
            </button>
        );
        const rows = []

        this.props.movies.forEach((movie) => {
            let g;
            for (g of this.props.genres) {
                if (movie.genre_ids.indexOf(g) === -1) {
                    return;
                }
            }
            rows.push(
                <div className={styles.movie}><Movie movies={this.props.movies} movie={movie} key={movie.id} baseURL={this.props.baseURL} imgSize={this.props.imgSize} onIndexChange={this.handleIndexChange} /></div>
            );
        });

        const genreNames = [];
        this.props.genres.forEach((genre) => {
            let gInfo;
            for (gInfo of this.props.genreInfo) {
                if (gInfo.id === genre) {
                    genreNames.push(<p>{gInfo.name}</p>);
                    break;
                }
            }
        });

        return (
            <div>
                <div>{genreNames}</div>
                <div className={styles.buttons}>
                    {buttons}
                </div>
                <hr></hr>
                <div className={styles.display}>
                    {rows}
                </div>
            </div>
        );
    }
}

export default MovieGallery;
